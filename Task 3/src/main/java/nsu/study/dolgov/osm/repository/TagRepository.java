package nsu.study.dolgov.osm.repository;

import nsu.study.dolgov.osm.model.TagDatabase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TagRepository extends JpaRepository<TagDatabase, Long> {

    @Transactional
    void deleteAllByNodeId(Long nodeId);
}
