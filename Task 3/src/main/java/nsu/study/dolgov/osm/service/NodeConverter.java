package nsu.study.dolgov.osm.service;

import nsu.study.dolgov.osm.model.NodeDTO;
import nsu.study.dolgov.osm.model.NodeDatabase;
import nsu.study.dolgov.osm.model.TagDatabase;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NodeConverter {
    public static NodeDTO convertToDTO(final NodeDatabase node) {
        final Map<String, String> tags = node.getTags().stream()
                .collect(Collectors.toMap(TagDatabase::getKey, TagDatabase::getValue));

        return NodeDTO.builder()
                .id(node.getId())
                .longitude(node.getLongitude())
                .latitude(node.getLatitude())
                .username(node.getUsername())
                .uid(node.getUid())
                .tags(tags)
                .build();
    }

    public static NodeDatabase convertToDb(final NodeDTO nodeDto) {
        final NodeDatabase node = NodeDatabase.builder()
                .latitude(nodeDto.getLatitude())
                .longitude(nodeDto.getLongitude())
                .username(nodeDto.getUsername())
                .uid(nodeDto.getUid())
                .build();

        final List<TagDatabase> tags =
                nodeDto.getTags().entrySet().stream()
                        .map(t -> TagDatabase.builder()
                                .key(t.getKey())
                                .value(t.getValue())
                                .node(node)
                                .build())
                        .collect(Collectors.toList());
        node.setTags(tags);
        return node;
    }
}
