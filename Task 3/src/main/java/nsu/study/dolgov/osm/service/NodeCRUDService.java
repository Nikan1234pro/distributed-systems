package nsu.study.dolgov.osm.service;

import lombok.RequiredArgsConstructor;
import nsu.study.dolgov.osm.model.NodeDTO;
import nsu.study.dolgov.osm.model.NodeDatabase;
import nsu.study.dolgov.osm.repository.NodeRepository;
import nsu.study.dolgov.osm.repository.TagRepository;
import org.springframework.stereotype.Service;
import javax.validation.constraints.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static nsu.study.dolgov.osm.service.NodeConverter.*;

@Service
@RequiredArgsConstructor
public class NodeCRUDService {
    private final NodeRepository nodeRepository;
    private final TagRepository tagRepository;

    @NotNull public NodeDTO create(@NotNull final NodeDTO nodeDTO) {
        final NodeDatabase savedNode = nodeRepository.save(convertToDb(nodeDTO));
        return convertToDTO(savedNode);
    }

    @NotNull public NodeDTO read(final long id) {
        final NodeDatabase node = nodeRepository.findById(id)
                .orElseThrow(DataNotFoundException::new);
        return convertToDTO(node);
    }

    @NotNull public NodeDTO update(final long id, @NotNull final NodeDTO nodeDTO) {
        final NodeDatabase nodeToSave = convertToDb(nodeDTO);
        final NodeDatabase oldNode = nodeRepository.findById(id)
                .orElseThrow(DataNotFoundException::new);
        nodeToSave.setId(oldNode.getId());

        /* Remove old nodes */
        tagRepository.deleteAllByNodeId(nodeToSave.getId());
        return convertToDTO(nodeRepository.save(nodeToSave));
    }

    public void delete(final long id) {
        final NodeDatabase nodeDb = nodeRepository.findById(id)
                .orElseThrow(DataNotFoundException::new);
        nodeRepository.delete(nodeDb);
    }

    @NotNull public List<NodeDTO> nearNodes(double latitude, double longitude, double radius) {
        return nodeRepository.getNearNodes(latitude, longitude, radius).stream()
                .map(NodeConverter::convertToDTO)
                .collect(Collectors.toList());
    }
}
