package nsu.study.dolgov.osm.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@Builder
public class NodeDTO {
    @NotNull
    private Long id;

    @NotNull
    private Double longitude;

    @NotNull
    private Double latitude;

    @NotNull
    private String username;

    @NotNull
    private Long uid;

    @NotNull
    private Map<String, String> tags;
}
