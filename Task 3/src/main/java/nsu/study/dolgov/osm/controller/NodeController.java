package nsu.study.dolgov.osm.controller;


import lombok.RequiredArgsConstructor;
import nsu.study.dolgov.osm.model.NodeDTO;
import nsu.study.dolgov.osm.service.NodeCRUDService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/node")
@RequiredArgsConstructor
public class NodeController {

    private final NodeCRUDService nodeService;

    @GetMapping("/{id}")
    public NodeDTO getNode(@PathVariable("id") final long id) {
        return nodeService.read(id);
    }

    @PostMapping
    NodeDTO createNode(@RequestBody final NodeDTO node) {
        return nodeService.create(node);
    }

    @PutMapping("/{id}")
    NodeDTO updateNode(@PathVariable("id") final Long id,
                       @RequestBody final NodeDTO node) {
        return nodeService.update(id, node);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") final long id) {
        nodeService.delete(id);
    }

    @GetMapping("/search")
    public List<NodeDTO> search(
            @RequestParam("latitude") Double latitude,
            @RequestParam("longitude") Double longitude,
            @RequestParam("radius") Double radius) {
        return nodeService.nearNodes(latitude, longitude, radius);
    }
}
