package nsu.study.dolgov.osm.model;

import lombok.*;
import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "nodes")
public class NodeDatabase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true, updatable = false, name="node_id")
    private long id;

    @Column(nullable = false, name = "latitude")
    private double latitude;

    @Column(nullable = false, name = "longitude")
    private double longitude;

    @Column(nullable = false, name = "username")
    private String username;

    @Column(nullable = false, name = "uid")
    private long uid;

    @OneToMany(
            cascade = CascadeType.ALL,
            mappedBy = "node")
    private List<TagDatabase> tags;
}
