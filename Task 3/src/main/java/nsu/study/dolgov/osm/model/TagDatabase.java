package nsu.study.dolgov.osm.model;

import lombok.*;
import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tags")
public class TagDatabase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private long id;

    @Column(nullable = false, name = "key")
    private String key;
    
    @Column(nullable = false, name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name = "node_id", nullable = false)
    private NodeDatabase node;
}
