package nsu.study.dolgov.osm.repository;

import nsu.study.dolgov.osm.model.NodeDatabase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodeRepository extends
        JpaRepository<NodeDatabase, Long> {

    @Query(
            value = "SELECT * FROM NODES WHERE " +
                    "earth_distance(ll_to_earth(55, 84), ll_to_earth(nodes.latitude, nodes.longitude)) " +
                    "< ?3 + earth_distance(ll_to_earth(?1, ?2), ll_to_earth(55, 84)) " +
                    "AND " +
                    "earth_distance(ll_to_earth(53, 82), ll_to_earth(nodes.latitude, nodes.longitude)) " +
                    "< ?3 + earth_distance(ll_to_earth(?1, ?2), ll_to_earth(53, 82)) " +
                    "AND " +
                    "earth_distance(ll_to_earth(?1, ?2), ll_to_earth(nodes.latitude, nodes.longitude)) " +
                    "< ?3 " +
                    "ORDER BY " +
                    "earth_distance(ll_to_earth(?1, ?2), ll_to_earth(nodes.latitude, nodes.longitude)) ASC",
            nativeQuery = true
    )
    List<NodeDatabase> getNearNodes(double latitude, double longitude, double radius);
}
