DROP INDEX IF EXISTS DISTANCE_TO_NODE_A;
DROP INDEX IF EXISTS DISTANCE_TO_NODE_B;

--A(55, 84) and B(53, 82)

CREATE INDEX DISTANCE_TO_NODE_A
ON NODES
(earth_distance(
	ll_to_earth(55, 84),
	ll_to_earth(NODES.latitude, NODES.longitude)
));

CREATE INDEX DISTANCE_TO_NODE_B
ON NODES
(earth_distance(
	ll_to_earth(53, 82),
	ll_to_earth(NODES.latitude, NODES.longitude)
));