package nsu.study.dolgov.osm;

import nsu.study.dolgov.osm.entity.NodeDatabase;
import nsu.study.dolgov.parser.XMLParser;
import nsu.study.dolgov.osm.entity.generated.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.sql.SQLException;

public class OSMProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(OSMProcessor.class);
    public static final String TAG = "node";

    private final NodeService nodeService;

    public OSMProcessor() throws SQLException {
        nodeService = new NodeService();
    }

    public void run(final InputStream stream)
            throws XMLStreamException, JAXBException {
        final XMLParser parser = new XMLParser(stream);

        LOGGER.debug("XML parsing started");
        parser.parse(TAG, Node.class, node -> {
            final NodeDatabase nodeDb = NodeDatabase.create(node);
            nodeService.processNodeBatch(nodeDb);
        });
        LOGGER.debug("XML parsing finished successfully");
    }
}
