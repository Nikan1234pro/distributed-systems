package nsu.study.dolgov.database.constants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DatabaseConstants {
    public static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/";
    public static final String DATABASE_USERNAME = "postgres";
    public static final String DATABASE_PASSWORD = "postgres";
    public static final String DATABASE_INIT = "init.sql";

    public static String getInitDDL() throws Exception {
        final URL scriptUrl = DatabaseConstants.class
                .getClassLoader()
                .getResource(DATABASE_INIT);

        if (scriptUrl == null) {
            throw new FileNotFoundException("Couldn't find file");
        }
        final byte[] encoded = Files.readAllBytes(Paths.get(scriptUrl.toURI()));
        return new String(encoded);
    }
}
