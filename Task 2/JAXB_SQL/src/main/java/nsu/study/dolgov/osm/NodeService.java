package nsu.study.dolgov.osm;

import nsu.study.dolgov.osm.dao.INodeDao;
import nsu.study.dolgov.osm.dao.ITagDao;
import nsu.study.dolgov.osm.dao.NodeDao;
import nsu.study.dolgov.osm.dao.TagDao;
import nsu.study.dolgov.osm.entity.NodeDatabase;
import nsu.study.dolgov.osm.entity.TagDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NodeService {
    private final INodeDao nodeDao;
    private final ITagDao tagDao;

    private static final int MAX_BUFFER_SIZE = 1000;
    private static final Logger LOGGER = LoggerFactory.getLogger(NodeService.class);

    private final List<NodeDatabase> nodeBuffer;

    public NodeService() throws SQLException {
        nodeBuffer = new ArrayList<>();
        nodeDao = new NodeDao();
        tagDao = new TagDao();
    }

    public NodeDatabase getNodeById(final long nodeId) {
        try {
            NodeDatabase node = nodeDao.findNodeById(nodeId);
            node.getTags().addAll(tagDao.findTagsByNodeId(nodeId));
            return node;
        }
        catch (final SQLException exception) {
            LOGGER.error(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

    public void processNode(final NodeDatabase node) {
        try {
            nodeDao.insertNode(node);
            for (final TagDatabase tag : node.getTags()) {
                tagDao.insertTag(tag);
            }
        }
        catch (final SQLException exception) {
            LOGGER.error(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

    public void processNodePrepared(final NodeDatabase node) {
        try {
            nodeDao.insertNodePrepared(node);
            for (final TagDatabase tag : node.getTags()) {
                tagDao.insertTagPrepared(tag);
            }
        }
        catch (final SQLException exception) {
            LOGGER.error(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

    public void processNodeBatch(final NodeDatabase node) {
        try {
            nodeBuffer.add(node);
            if (nodeBuffer.size() == MAX_BUFFER_SIZE) {
                flush();
            }
        }
        catch (final SQLException exception) {
            LOGGER.error(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

    private void flush() throws SQLException {
        nodeDao.insertNodeBatch(nodeBuffer);
        final List<TagDatabase> tags = nodeBuffer.stream()
                .flatMap(node -> node.getTags().stream())
                .collect(Collectors.toList());
        tagDao.insertTagBatch(tags);
        nodeBuffer.clear();
    }
}
