package nsu.study.dolgov.osm.dao;

import nsu.study.dolgov.osm.entity.TagDatabase;

import java.sql.SQLException;
import java.util.List;

public interface ITagDao {
    List<TagDatabase> findTagsByNodeId(final long nodeId) throws SQLException;

    void insertTag(final TagDatabase tag) throws SQLException;
    void insertTagPrepared(final TagDatabase tag) throws SQLException;
    void insertTagBatch(final List<TagDatabase> tags) throws SQLException;
}
