package nsu.study.dolgov;

import nsu.study.dolgov.database.constants.DatabaseConstants;
import nsu.study.dolgov.database.DatabaseController;
import nsu.study.dolgov.database.utils.DatabaseUtils;
import nsu.study.dolgov.osm.OSMProcessor;
import nsu.study.dolgov.osm.constants.OSMConstants;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    private static final DatabaseController controller = DatabaseController.getInstance();

    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.error("Too few args");
            return;
        }
        final String fileName = args[0];

        try (final InputStream inputStream = new BufferedInputStream(Files.newInputStream(Paths.get(fileName)));
             final InputStream bZip2InputStream = new CompressorStreamFactory()
                     .createCompressorInputStream(OSMConstants.ARCHIVE_TYPE, inputStream)) {

            controller.connect(
                    DatabaseConstants.DATABASE_URL,
                    DatabaseConstants.DATABASE_USERNAME,
                    DatabaseConstants.DATABASE_PASSWORD);

            /* Run processor */
            final OSMProcessor osmProcessor = new OSMProcessor();
            osmProcessor.run(bZip2InputStream);
        }
        catch (final Exception exception) {
            LOGGER.error(exception.getMessage());
        } finally {
            try {
                controller.closeConnection();
            } catch (final SQLException exception) {
                LOGGER.error(exception.getMessage());
            }
        }
    }
}
