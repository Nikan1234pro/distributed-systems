package nsu.study.dolgov.osm.dao;

import nsu.study.dolgov.database.DatabaseController;
import nsu.study.dolgov.osm.entity.NodeDatabase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static nsu.study.dolgov.database.utils.DatabaseUtils.quotedString;

public class NodeDao implements INodeDao {

    private static final DatabaseController controller =
            DatabaseController.getInstance();

    private static final String SQL_INSERT =
            "INSERT INTO NODES(node_id, latitude, longitude, username, uid) VALUES (?, ?, ?, ?, ?)";

    private static final String SQL_SELECT =
            "SELECT node_id, latitude, longitude, username, uid FROM NODES WHERE node_id = ?";

    private final PreparedStatement insertStatement;
    private final PreparedStatement selectStatement;

    public NodeDao() throws SQLException {
            final Connection connection = controller.getConnection();
            insertStatement = connection.prepareStatement(SQL_INSERT);
            selectStatement = connection.prepareStatement(SQL_SELECT);
        }

    @Override
    public NodeDatabase findNodeById(final long nodeId) throws SQLException {
        selectStatement.setLong(1, nodeId);
        final ResultSet resultSet = selectStatement.executeQuery();
        return resultSet.next() ? createNode(resultSet) : null;
    }

    @Override
    public void insertNode(final NodeDatabase node) throws SQLException {
        final String sql = "INSERT INTO NODES(node_id, latitude, longitude, username, uid) VALUES("
                + node.getNodeId() + ", "
                + node.getLatitude() + ", "
                + node.getLongitude() + ", "
                + quotedString(node.getUsername()) + ", "
                + node.getUid() + ")";
        final Statement statement = controller.getConnection().createStatement();
        statement.execute(sql);
    }

    @Override
    public void insertNodePrepared(final NodeDatabase node) throws SQLException {
        prepareInsertStatement(insertStatement, node).execute();
    }

    @Override
    public void insertNodeBatch(final List<NodeDatabase> nodes) throws SQLException {
        for (final NodeDatabase node : nodes) {
            prepareInsertStatement(insertStatement, node).addBatch();
        }
        insertStatement.executeBatch();
    }

    private static PreparedStatement prepareInsertStatement(final PreparedStatement statement,
                                                            final NodeDatabase node) throws SQLException {
        statement.setLong(1, node.getNodeId());
        statement.setDouble(2, node.getLatitude());
        statement.setDouble(3, node.getLongitude());
        statement.setString(4, node.getUsername());
        statement.setLong(5, node.getUid());
        return statement;
    }

    private static NodeDatabase createNode(final ResultSet resultSet) throws SQLException {
        final long node_id = resultSet.getLong(1);
        final double latitude = resultSet.getDouble(2);
        final double longitude = resultSet.getDouble(3);
        final String username = resultSet.getString(4);
        final long uid = resultSet.getLong(5);
        return new NodeDatabase(node_id, latitude, longitude, username, uid, new ArrayList<>());
    }
}
