package nsu.study.dolgov.osm.dao;

import nsu.study.dolgov.database.DatabaseController;
import nsu.study.dolgov.osm.entity.TagDatabase;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static nsu.study.dolgov.database.utils.DatabaseUtils.quotedString;

public class TagDao implements ITagDao {

    private static final DatabaseController controller =
            DatabaseController.getInstance();

    private static final String SQL_INSERT =
            "INSERT INTO TAGS(node_id, key, value) VALUES (?, ?, ?)";

    private static final String SQL_SELECT =
            "SELECT node_id, key, value FROM TAGS where node_id = ?";

    private final PreparedStatement insertStatement;
    private final PreparedStatement selectStatement;

    public TagDao() throws SQLException {
        final Connection connection = controller.getConnection();
        insertStatement = connection.prepareStatement(SQL_INSERT);
        selectStatement = connection.prepareStatement(SQL_SELECT);
    }

    @Override
    public List<TagDatabase> findTagsByNodeId(final long nodeId) throws SQLException {
        selectStatement.setLong(1, nodeId);
        final ResultSet resultSet = selectStatement.executeQuery();
        return createTags(resultSet);
    }

    @Override
    public void insertTag(final TagDatabase tag) throws SQLException {
        final String sql = "INSERT INTO TAGS(node_id, key, value) VALUES ("
                + tag.getNodeId() + ", "
                + quotedString(tag.getKey()) + ", "
                + quotedString(tag.getValue()) + ")";
        final Statement statement = controller.getConnection().createStatement();
        statement.execute(sql);
    }

    @Override
    public void insertTagPrepared(final TagDatabase tag) throws SQLException {
        prepareInsertStatement(insertStatement, tag).execute();
    }

    @Override
    public void insertTagBatch(final List<TagDatabase> tags) throws SQLException {
        for (final TagDatabase tag : tags) {
            prepareInsertStatement(insertStatement, tag).addBatch();
        }
        insertStatement.executeBatch();
    }

    private static PreparedStatement prepareInsertStatement(final PreparedStatement statement,
                                                            final TagDatabase tag) throws SQLException {
        statement.setLong(1, tag.getNodeId());
        statement.setString(2, tag.getKey());
        statement.setString(3, tag.getValue());
        return statement;
    }

    private static List<TagDatabase> createTags(final ResultSet resultSet) throws SQLException {
        final List<TagDatabase> tags = new LinkedList<>();
        while (resultSet.next()) {
            final TagDatabase tag = new TagDatabase(
                    resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3)
            );
            tags.add(tag);
        }
        return tags;
    }
}
