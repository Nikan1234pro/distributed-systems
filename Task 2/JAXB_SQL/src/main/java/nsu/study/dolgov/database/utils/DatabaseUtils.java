package nsu.study.dolgov.database.utils;

public class DatabaseUtils {

    public static String quotedString(final String string) {
        final char quote = '\'';

        int begin = 0;
        int end = string.indexOf(quote, begin);

        StringBuilder builder = new StringBuilder();
        while (end >= 0) {
            builder.append(string, begin, end + 1);
            builder.append(quote);

            begin = end + 1;
            end = string.indexOf(quote, begin);
        }
        builder.append(string.substring(begin));
        return quote + builder.toString() + quote;
    }
}
