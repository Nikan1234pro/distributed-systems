package nsu.study.dolgov.osm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nsu.study.dolgov.osm.entity.generated.Node;

@AllArgsConstructor
public class TagDatabase {

    @Setter
    @Getter
    private long nodeId;

    @Setter
    @Getter
    private String key;

    @Setter
    @Getter
    private String value;

    @Override
    public String toString() {
        return "{" + key + ", " + value + "}";
    }


    static TagDatabase create(final Node node, final Node.Tag tag) {
        return new TagDatabase(node.getId().longValue(), tag.getK(), tag.getV());
    }
}
