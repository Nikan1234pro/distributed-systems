package nsu.study.dolgov.osm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import nsu.study.dolgov.osm.entity.generated.Node;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class NodeDatabase {

    @Setter
    @Getter
    private long nodeId;

    @Setter
    @Getter
    private double latitude;

    @Setter
    @Getter
    private double longitude;

    @Setter
    @Getter
    private String username;

    @Setter
    @Getter
    private long uid;

    @Setter
    @Getter
    private List<TagDatabase> tags;


    public static NodeDatabase create(final Node node) {
        final List<TagDatabase> tags = node.getTag()
                .stream()
                .map(tag -> TagDatabase.create(node, tag))
                .collect(Collectors.toList());

        return new NodeDatabase(
                node.getId().longValue(),
                node.getLat(),
                node.getLon(),
                node.getUser(),
                node.getUid().longValue(),
                tags);
    }
}
