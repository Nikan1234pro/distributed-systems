package nsu.study.dolgov.osm.dao;

import nsu.study.dolgov.osm.entity.NodeDatabase;

import java.sql.SQLException;
import java.util.List;

public interface INodeDao {
    NodeDatabase findNodeById(final long nodeId) throws SQLException;

    void insertNode(final NodeDatabase node) throws SQLException;
    void insertNodePrepared(final NodeDatabase node) throws SQLException;
    void insertNodeBatch(final List<NodeDatabase> nodes) throws SQLException;
}
