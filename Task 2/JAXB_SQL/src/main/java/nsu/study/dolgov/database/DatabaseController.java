package nsu.study.dolgov.database;

import nsu.study.dolgov.database.constants.DatabaseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

public class DatabaseController {
    Connection connection;

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseController.class);
    private static volatile DatabaseController instance;

    private DatabaseController() {
        this.connection = null;
    }

    public static DatabaseController getInstance() {
        DatabaseController local_instance = instance;
        if (instance == null) {
            synchronized (DatabaseController.class) {
                local_instance = instance;
                if (instance == null) {
                    instance = local_instance = new DatabaseController();
                }
            }
        }
        return local_instance;
    }

    public void connect(String url, String login, String password) throws SQLException {
        Properties props = new Properties();
        props.setProperty("user", login);
        props.setProperty("password", password);

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);

        LOGGER.debug("Opening connection...");
        connection = DriverManager.getConnection(url, props);

        try {
            final Statement statement = connection.createStatement();
            statement.execute(DatabaseConstants.getInitDDL());
        }
        catch (final Exception exception) {
            LOGGER.error("Couldn't run init script: " + exception.getMessage());
        }
    }

    public void closeConnection() throws SQLException {
        if (connection != null) {
            LOGGER.debug("Closing connection...");
            connection.close();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
