package nsu.study.dolgov.parser;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.util.function.Consumer;

public class XMLParser {

    private final InputStream inputStream;

    public XMLParser(final InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public <T> void parse(final String tagName,
                          final Class<T> declaredType,
                          final Consumer<T> consumer) throws XMLStreamException, JAXBException {

        XMLStreamReader reader = null;
        try {
            reader = XMLInputFactory.newInstance().createXMLStreamReader(inputStream);
            final JAXBContext jaxbContext = JAXBContext.newInstance(declaredType);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            while (reader.hasNext()) {
                final int event = reader.next();
                if (event == XMLEvent.START_ELEMENT && tagName.equals(reader.getLocalName())) {
                    final T value = unmarshaller.unmarshal(reader, declaredType).getValue();
                    consumer.accept(value);
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
