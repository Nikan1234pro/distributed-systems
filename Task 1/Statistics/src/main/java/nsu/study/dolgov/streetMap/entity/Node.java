package nsu.study.dolgov.streetMap.entity;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class Node {

    @Setter
    @Getter
    @XmlAttribute
    private String user;

    @Setter
    @Getter
    @XmlElement(name = "tag")
    private List<Tag> tags = null;
}
