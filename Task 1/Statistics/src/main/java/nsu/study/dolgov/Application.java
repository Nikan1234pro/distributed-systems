package nsu.study.dolgov;

import nsu.study.dolgov.streetMap.OpenStreetMapStatistics;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;

public class Application {
    public static final String ARCHIVE_TYPE = "BZIP2";
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.error("Too few args");
            return;
        }
        final String fileName = args[0];

        try (final InputStream inputStream = new BufferedInputStream(Files.newInputStream(Paths.get(fileName)));
             final InputStream bZip2InputStream = new CompressorStreamFactory()
                     .createCompressorInputStream(ARCHIVE_TYPE, inputStream)) {

            OpenStreetMapStatistics statistics = new OpenStreetMapStatistics(bZip2InputStream);
            System.out.println("\n-------Top user edits-------");
            printSorted(statistics.getUserEdits());

            System.out.println("\n-------Top tags-------");
            printSorted(statistics.getTagNodes());
        }
        catch (final Exception exception) {
            LOGGER.error("Caught exception: " + exception.getMessage());
        }
    }

    private static <K, V extends Comparable<? super V>> void printSorted(final Map<K, V> map) {
        map.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
    }
}
