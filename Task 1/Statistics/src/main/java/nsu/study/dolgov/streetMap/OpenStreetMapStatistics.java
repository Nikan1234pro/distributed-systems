package nsu.study.dolgov.streetMap;

import lombok.Getter;
import nsu.study.dolgov.parser.XMLParser;
import nsu.study.dolgov.streetMap.entity.Node;
import nsu.study.dolgov.streetMap.entity.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OpenStreetMapStatistics {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpenStreetMapStatistics.class);

    public static final String TAG = "node";

    @Getter
    private final Map<String, Integer> userEdits;

    @Getter
    private final Map<String, Integer> tagNodes;

    public OpenStreetMapStatistics(final InputStream stream)
            throws XMLStreamException, JAXBException {
        this.userEdits = new HashMap<>();
        this.tagNodes = new HashMap<>();

        final XMLParser parser = new XMLParser(stream);

        LOGGER.debug("XML parsing started");
        parser.parse(TAG, Node.class, node -> {
            /* Increment user edits */
            userEdits.merge(node.getUser(), 1, Integer::sum);

            /* For each tag do increment */
            final List<Tag> tags = node.getTags();
            if (tags != null) {
                for (final Tag tag : tags) {
                    tagNodes.merge(tag.getKey(), 1, Integer::sum);
                }
            }
        });
        LOGGER.debug("XML parsing finished successfully");
    }
}
