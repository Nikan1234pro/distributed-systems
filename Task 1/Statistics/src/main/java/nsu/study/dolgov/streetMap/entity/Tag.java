package nsu.study.dolgov.streetMap.entity;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode
public class Tag {

    @Setter
    @Getter
    @XmlAttribute(name = "k")
    private String key;


    @Setter
    @Getter
    @XmlAttribute(name = "v")
    private String value;

    @Override
    public String toString() {
        return "{" + key + ", " + value + "}";
    }
}
